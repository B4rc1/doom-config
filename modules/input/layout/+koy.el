;;; input/layout/+koy.el -*- lexical-binding: t; -*-

;;
;;; Initializers

;; TODO Separate each package into their own hook, so users can
;;      enable/disable/add their own per-package remappings.

(defun +layout-remap-keys-for-koy-h ()
  (setq avy-keys '(?h ?a ?e ?i ?u ?d ?t ?r ?n ?s))

  ;; :ui window-select settings, ignoring +numbers flag for now
  (after! ace-window
    (setq aw-keys '(?a ?u ?i ?e ?, ?c ?t ?s ?r ?n)))
  (after! switch-window
    (setq switch-window-shortcut-style 'qwerty
          switch-window-qwerty-shortcuts '("a" "u" "i" "e" "," "c" "t" "s" "r"))))

(defun +layout-remap-evil-keys-for-koy-h ()
  (evil-collection-translate-key nil 'doom-leader-map
    "." "w"
    "w" ".")

  (after! evil
    (map! :leader
      ".d" 'evil-window-left
      ".h" 'evil-window-delete
		  ".t" 'evil-window-down
		  ".r" 'evil-window-up
		  ".n" 'evil-window-right

      ".D" '+evil/window-move-left
		  ".T" '+evil/window-move-down
		  ".R" '+evil/window-move-up
		  ".N" '+evil/window-move-right))

  (setq evil-escape-key-sequence nil)
  (setq evil-markdown-movement-bindings '((up . "r")
                                          (down . "t")
                                          (left . "d")
                                          (right . "n"))
        evil-org-movement-bindings '((up . "r")
                                     (down . "t")
                                     (left . "d")
                                     (right . "n")))
  (+layout-koy-rotate-tr-bare-keymap '(read-expression-map))
  (+layout-koy-rotate-bare-keymap '(evil-window-map) )
  (+layout-koy-rotate-evil-keymap )
  ;; Remap the visual-mode-map bindings if necessary
  ;; See https://github.com/emacs-evil/evil/blob/7d00c23496c25a66f90ac7a6a354b1c7f9498162/evil-integration.el#L478-L501
  ;; Using +layout-koy-rotate-keymaps is impossible because `evil-define-minor-mode-key' doesn't
  ;; provide an actual symbol to design the new keymap with, and instead stuff the keymap in
  ;; an auxiliary-auxiliary `minor-mode-map-alist'-like variable.
  (after! evil-integration
    (when evil-respect-visual-line-mode
      (map! :map visual-line-mode-map
            :m "t"  #'evil-next-visual-line
            ;; _Not_ remapping gj and gk because they aren't remapped
            ;; consistently across all Emacs.
            :m "r"  #'evil-previous-visual-line
            :m "g$" #'evil-end-of-line)))

  (after! treemacs
    (+layout-koy-rotate-tr-bare-keymap '(evil-treemacs-state-map)))
  (after! (:or helm ivy selectrum icomplete)
    (+layout-koy-rotate-keymaps
     '(minibuffer-local-map
       minibuffer-local-ns-map
       minibuffer-local-completion-map
       minibuffer-local-must-match-map
       minibuffer-local-isearch-map
       read-expression-map))
    (+layout-koy-rotate-bare-keymap
     '(minibuffer-local-map
       minibuffer-local-ns-map
       minibuffer-local-completion-map
       minibuffer-local-must-match-map
       minibuffer-local-isearch-map
       read-expression-map)
     ))
  (after! vertico
    (+layout-koy-rotate-bare-keymap '(vertico-map minibuffer-local-map minibuffer-mode-map))
    (+layout-koy-rotate-keymaps '(vertico-map minibuffer-local-map minibuffer-mode-map))
    )
  ;; (after! swiper
  ;;   (map! :map swiper-map "C-s" nil))
  (after! helm
    (+layout-koy-rotate-bare-keymap '(helm-map) )
    (+layout-koy-rotate-keymaps '(helm-map)))
  (after! helm-rg
    (+layout-koy-rotate-bare-keymap '(helm-rg-map) )
    (+layout-koy-rotate-keymaps '(helm-rg-map)))
  (after! helm-files
    (+layout-koy-rotate-bare-keymap '(helm-read-file-map) )
    (+layout-koy-rotate-keymaps '(helm-read-file-map)))
  (after! selectrum
    (+layout-koy-rotate-bare-keymap '(selectrum-minibuffer-map) )
    (+layout-koy-rotate-keymaps '(selectrum-minibuffer-map)))
  (after! company
    (+layout-koy-rotate-bare-keymap '(company-active-map company-search-map) ))
  (after! evil-snipe
    (message "after evil snipe")
    (+layout-koy-rotate-keymaps
     '(evil-snipe-local-mode-map evil-snipe-override-local-mode-map)))
  (after! eshell
    (add-hook 'eshell-first-time-mode-hook (lambda () (+layout-koy-rotate-keymaps '(eshell-mode-map))) 99))
  (after! lsp-ui
    (+layout-koy-rotate-tr-bare-keymap '(lsp-ui-peek-mode-map)))
  (after! org
    (defadvice! doom-koy--org-completing-read (&rest args)
      "Completing-read with SPACE being a normal character, and C-c mapping left alone."
      :override #'org-completing-read
      (let ((enable-recursive-minibuffers t)
            (minibuffer-local-completion-map
             (copy-keymap minibuffer-local-completion-map)))
        (define-key minibuffer-local-completion-map " " 'self-insert-command)
        (define-key minibuffer-local-completion-map "?" 'self-insert-command)
        (apply #'completing-read args)))
    ;; Finalizing an Org-capture become `C-l C-c` (or `C-r C-c`) on top of `ZZ`
    (+layout-koy-rotate-bare-keymap '(org-capture-mode-map) ))
  (after! (evil org evil-org)
    (+layout-koy-rotate-keymaps '(evil-org-mode-map))
  (after! (evil org-agenda evil-org-agenda)
    (+layout-koy-rotate-bare-keymap '(org-agenda-keymap) )
    (+layout-koy-rotate-keymaps '(evil-org-agenda-mode-map)))
  (after! (evil info)
    ;; Without this, "s" stays mapped to 'Info-search (in the "global"
    ;; `Info-mode-map') and takes precedence over the evil command to go up one
    ;; line (remapped in `Info-mode-normal-state-map').  Same for "t" that is
    ;; `Info-top-node' in the "global" `Info-mode-map'
    (map! :map Info-mode-map
          "d" nil
          "t" nil
          "r" nil
          "n" nil))
  (after! (evil magit)
    (+layout-koy-rotate-tr-bare-keymap
     '(magit-mode-map
       magit-diff-section-base-map
       magit-staged-section-map
       magit-unstaged-section-map
       magit-untracked-section-map))
    ;; Without this, "s" is mapped to 'magit-delete-thing (the old "k" for "kill") and
    ;; takes precedence over the evil command to go up one line
    ;; :nv doesn't work on this, needs to be the bare map.
    ;; This is the output of `describe-function 'magit-delete-thing` when we add :nv or :nvm
    ;; Key Bindings
    ;;   evil-collection-magit-mode-map-backup-map <normal-state> x
    ;;   evil-collection-magit-mode-map-backup-map <visual-state> x
    ;;   evil-collection-magit-mode-map-backup-map k
    ;;   evil-collection-magit-mode-map-normal-state-backup-map x
    ;;   evil-collection-magit-mode-map-visual-state-backup-map x
    ;;   magit-mode-map <normal-state> x
    ;;   magit-mode-map <visual-state> x
    ;;   magit-mode-map s
    (map! :map magit-mode-map 
	  "t" nil
	  "r" nil
	  "n" nil
	  "s" nil)
    ;; Even though magit bindings are part of evil-collection now, the hook only
    ;; runs on `evil-collection-magit-maps', which is way to short to cover all
    ;; usages. The hook is run manually on other maps
    ;; NOTE `magit-mode-map' is last because other keymaps inherit from it.
    ;;      Therefore to prevent a "double rotation" issue, `magit-mode-map' is
    ;;      changed last.
    (+layout-koy-rotate-keymaps
     '(magit-cherry-mode-map
       magit-blob-mode-map
       magit-diff-mode-map
       magit-log-mode-map
       magit-log-select-mode-map
       magit-reflog-mode-map
       magit-status-mode-map
       magit-log-read-revs-map
       magit-process-mode-map
       magit-refs-mode-map
       magit-mode-map)))
  ))


;;
;;; Bootstrap

(+layout-remap-keys-for-koy-h)
(when (featurep! :editor evil)
  (+layout-remap-evil-keys-for-koy-h)
  (add-hook! 'evil-collection-setup-hook
    (defun +layout-koy-rotate-evil-collection-keymap (_mode mode-keymaps &rest _rest)
      (+layout-koy-rotate-keymaps mode-keymaps))))
