;;; input/layout/autoload/koy.el -*- lexical-binding: t; -*-
;;;###if (featurep! +koy)

;;;###autoload
(defun +layout-koy-rotate-tr-bare-keymap (keymaps)
  "Rotate [jk] with [tr] in KEYMAP."
  (evil-collection-translate-key nil keymaps
    "t" "j"
    "T" "J"
    "r" "k"
    "R" "K"
    "j" "t"
    "J" "T"
    "k" "r"
    "K" "R"
    (kbd "C-t") (kbd "C-j")
    (kbd "C-r") (kbd "C-k")
    (kbd "C-j") (kbd "C-t")
    (kbd "C-k") (kbd "C-r")
    (kbd "M-t") (kbd "M-j")
    (kbd "M-r") (kbd "M-k")
    (kbd "M-j") (kbd "M-t")
    (kbd "M-k") (kbd "M-r")
    (kbd "C-S-t") (kbd "C-S-j")
    (kbd "C-S-r") (kbd "C-S-k")
    (kbd "C-S-j") (kbd "C-S-t")
    (kbd "C-S-k") (kbd "C-S-r")
    (kbd "M-S-t") (kbd "M-S-j")
    (kbd "M-S-r") (kbd "M-S-k")
    (kbd "M-S-j") (kbd "M-S-t")
    (kbd "M-S-k") (kbd "M-S-r")))

;;;###autoload
(defun +layout-koy-rotate-w-bare-keymap (keymaps)
  "Rotate [w] with [.] in KEYMAP."
  (evil-collection-translate-key nil keymaps
    "." "w"
    "•" "W"
    (kbd "C-.") (kbd "C-w")
    (kbd "M-.") (kbd "M-w")
    (kbd "C-S-.") (kbd "C-S-w")
    (kbd "M-S-.") (kbd "M-S-w")))

;;;###autoload
(defun +layout-koy-rotate-dn-bare-keymap (keymaps)
  "Rotate [hl] with [dn] in KEYMAP."
  (evil-collection-translate-key nil keymaps
    "d" "h"
    "D" "H"
    "n" "l"
    "N" "L"
    (kbd "C-d") (kbd "C-h")
    (kbd "C-n") (kbd "C-l")
    (kbd "M-d") (kbd "M-h")
    (kbd "M-n") (kbd "M-l")
    (kbd "C-S-d") (kbd "C-S-h")
    (kbd "C-S-n") (kbd "C-S-l")
    (kbd "M-S-d") (kbd "M-S-h")
    (kbd "M-S-n") (kbd "M-S-l"))
    (evil-collection-translate-key nil keymaps
      "h" "d"
      "H" "D"
      "l" "n"
      "L" "N"
      (kbd "C-h") (kbd "C-d")
      (kbd "C-l") (kbd "C-n")
      (kbd "M-h") (kbd "M-d")
      (kbd "M-l") (kbd "M-n")
      (kbd "C-S-h") (kbd "C-S-d")
      (kbd "C-S-l") (kbd "C-S-n")
      (kbd "M-S-h") (kbd "M-S-d")
      (kbd "M-S-l") (kbd "M-S-n")))

;;;###autoload
(defun +layout-koy-rotate-bare-keymap (keymaps)
  "Rotate [hjklw] with [dtrn.] in KEYMAP."
  (+layout-koy-rotate-dn-bare-keymap keymaps)
  (+layout-koy-rotate-tr-bare-keymap keymaps)
  (+layout-koy-rotate-w-bare-keymap keymaps))

;;;###autoload
(defun +layout-koy-rotate-evil-keymap ()
  "Remap evil-{normal,operator,motion,...}-state-map to be more natural with"
    (evil-collection-translate-key nil
      '(evil-normal-state-map evil-motion-state-map evil-visual-state-map evil-operator-state-map)
      "d" "h" 
      "h" "d"
      "t" "j"
      "j" "t"
      "r" "k"
      "k" "r"
      "n" "l"
      "l" "n"

      "D" "H" 
      "H" "D"
      "T" "J"
      "J" "T"
      "R" "K"
      "K" "R"
      "N" "L"
      "L" "N"
      )
;    (evil-collection-translate-key nil '(evil-insert-state-map)
;      (kbd "C-c") (kbd "C-h")
;      (kbd "C-C") (kbd "C-H")
;      (kbd "C-t") (kbd "C-j")
;      (kbd "C-T") (kbd "C-J")
;      (kbd "C-s") (kbd "C-k")
;      (kbd "C-S") (kbd "C-K")
;      (kbd "C-r") (kbd "C-l")
;      (kbd "C-R") (kbd "C-L")
;      (kbd "C-j") (kbd "C-t")
;      (kbd "C-J") (kbd "C-T")
;      (kbd "C-k") (kbd "C-s")
;      (kbd "C-K") (kbd "C-S")
;      (kbd "C-h") (kbd "C-r")
;      (kbd "C-H") (kbd "C-R")
;      (kbd "C-l") (kbd "C-c")
;      (kbd "C-L") (kbd "C-C"))

    ;; [W] -> [.]
    (evil-collection-translate-key nil
      '(evil-normal-state-map evil-motion-state-map evil-operator-state-map)
     (kbd "C-.") (kbd "C-w")))

;;;###autoload
(defun +layout-koy-rotate-keymaps (keymaps )
  "Remap evil-collection keybinds in KEYMAPS for Bépo keyboard keyboard layouts."
    (evil-collection-translate-key '(normal motion visual operator) keymaps
      "d" "h"
      "D" "H"
      "t" "j"
      "T" "J"
      "r" "k"
      "R" "K"
      "n" "l"
      "N" "L"

      "h" "d"
      "H" "D"
      "j" "t"
      "J" "T"
      "k" "r"
      "K" "R"
      "l" "n"
      "L" "N"
      ;;;;;;;;;;;;;;;;;;;;;;;;;;
      (kbd "C-d") (kbd "C-h")
      (kbd "C-D") (kbd "C-H")
      (kbd "C-t") (kbd "C-j")
      (kbd "C-T") (kbd "C-J")
      (kbd "C-r") (kbd "C-k")
      (kbd "C-R") (kbd "C-K")
      (kbd "C-n") (kbd "C-l")
      (kbd "C-N") (kbd "C-r")

      (kbd "C-h") (kbd "C-d")
      (kbd "C-H") (kbd "C-D")
      (kbd "C-j") (kbd "C-t")
      (kbd "C-J") (kbd "C-T")
      (kbd "C-k") (kbd "C-r")
      (kbd "C-K") (kbd "C-S")
      (kbd "C-n") (kbd "C-l")
      (kbd "C-N") (kbd "C-L")
      ;;;;;;;;;;;;;;;;;;;;;;;;;;
      (kbd "M-d") (kbd "M-h")
      (kbd "M-D") (kbd "M-H")
      (kbd "M-t") (kbd "M-j")
      (kbd "M-T") (kbd "M-J")
      (kbd "M-r") (kbd "M-k")
      (kbd "M-R") (kbd "M-K")
      (kbd "M-n") (kbd "M-l")
      (kbd "M-N") (kbd "M-r")

      (kbd "M-h") (kbd "M-d")
      (kbd "M-H") (kbd "M-D")
      (kbd "M-j") (kbd "M-t")
      (kbd "M-J") (kbd "M-T")
      (kbd "M-k") (kbd "M-r")
      (kbd "M-K") (kbd "M-S")
      (kbd "M-n") (kbd "M-l")
      (kbd "M-N") (kbd "M-L"))
    (evil-collection-translate-key '(insert) keymaps
      (kbd "C-d") (kbd "C-h")
      (kbd "C-D") (kbd "C-H")
      (kbd "C-t") (kbd "C-j")
      (kbd "C-T") (kbd "C-J")
      (kbd "C-r") (kbd "C-k")
      (kbd "C-R") (kbd "C-K")
      (kbd "C-n") (kbd "C-l")
      (kbd "C-N") (kbd "C-r")

      (kbd "C-h") (kbd "C-d")
      (kbd "C-H") (kbd "C-D")
      (kbd "C-j") (kbd "C-t")
      (kbd "C-J") (kbd "C-T")
      (kbd "C-k") (kbd "C-r")
      (kbd "C-K") (kbd "C-S")
      (kbd "C-n") (kbd "C-l")
      (kbd "C-N") (kbd "C-L")
      ;;;;;;;;;;;;;;;;;;;;;;;;;;
      (kbd "M-d") (kbd "M-h")
      (kbd "M-D") (kbd "M-H")
      (kbd "M-t") (kbd "M-j")
      (kbd "M-T") (kbd "M-J")
      (kbd "M-r") (kbd "M-k")
      (kbd "M-R") (kbd "M-K")
      (kbd "M-n") (kbd "M-l")
      (kbd "M-N") (kbd "M-r")

      (kbd "M-h") (kbd "M-d")
      (kbd "M-H") (kbd "M-D")
      (kbd "M-j") (kbd "M-t")
      (kbd "M-J") (kbd "M-T")
      (kbd "M-k") (kbd "M-r")
      (kbd "M-K") (kbd "M-S")
      (kbd "M-n") (kbd "M-l")
      (kbd "M-N") (kbd "M-L"))

    ;; [W] -> [.]
    (evil-collection-translate-key nil
      '(evil-normal-state-map evil-motion-state-map evil-operator-state-map)
     (kbd "C-.") (kbd "C-w")))
