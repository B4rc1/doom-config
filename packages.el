;; -*- no-byte-compile: t; -*-
;;; packages.el

(package! org-super-agenda)
(package! alarm-clock)
(package! org-drill)
